<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StoreController;
use App\Models\Stores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('user')->group(function () {
    Route::controller(UserController::class)->group(function () {
        Route::post('/login', 'login');
        Route::post('/logout', 'logout');
    });
});

Route::middleware('auth:api')->prefix('product')->group(function () {
    Route::controller(ProductController::class)->group(function () {
        Route::get('/list', 'index');
        Route::get('/get', 'show');
        Route::post('/create', 'create');
        Route::put('/update', 'update');
        Route::delete('/delete', 'destroy');
    });
});

Route::middleware('auth:api')->prefix('store')->group(function () {
    Route::controller(StoreController::class)->group(function () {
        Route::get('/list', 'index');
        Route::get('/get', 'show');
        Route::post('/create', 'create');
        Route::put('/update', 'update');
        Route::delete('/delete', 'destroy');
    });
});

