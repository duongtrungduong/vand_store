<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected function successResponse($data, $msg, $code = 200)
    {
        $response = [
            'Result'  => true,
            'Message' => $msg,
            'Data'    => $data,
        ];
        return response()->json($response, $code);
    }


    /**
     * Standard API Error Response
     * @param $Result
     * @param $Message
     * @param $Data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse($msg = null, $code = 400, $errors = [])
    {
        $response = [
            'Result'  => false,
            'Message' => $msg,
            'Data'    => $errors,
        ];
        return response()->json($response, $code);
    }
}
