<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoresRequest;
use App\Models\Products;
use App\Models\Stores;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     */


    public function index(Request $request)
    {
        //
        $user = Auth::user();

        $query = Stores::where('user_id', $user->id);
        if ($request->input('name', '') != '') {
            $query->where('name', 'like', '%' . $request->input('name', '') . '%');
        };

        $data = $query->paginate($request->input('limit', 10));
        return $this->successResponse($data, __('Retrieving success'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(StoresRequest $request)
    {
        $store = [
            'name' => $request->name,
            'address' => $request->address,
            'user_id' =>  Auth::user()->id,
        ];

        try {
            $createStore = Stores::create(
                $store
            );
            return $this->successResponse(['id' => $createStore->id], __('Create success'));
        } catch (\Exception $e) {
            return $this->errorResponse(__("Internal Server Error"), 400, $e);
        }
    }


    public function show(Request $request)
    {
        $store = Stores::where([
            ['user_id', '=', Auth::user()->id],
            ['id', '=', $request->id],
        ])->first();

        if (!$store) {
            return $this->errorResponse(__("Store not found"), 404, null);
        }

        return $this->successResponse($store, __('Retrieving success'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoresRequest $request)
    {

        $store = Stores::where([
            ['user_id', '=', Auth::user()->id],
            ['id', '=', $request->id],
        ])->first();

        if (!$store) {
            return $this->errorResponse(__("Store not found"), 404, null);
        }

        try {
            $store->update($request->except('user_id'));
            return $this->successResponse(['id' => $store->id], __('Update success'));
        } catch (\Exception $e) {
            return $this->errorResponse(__("Internal Server Error"), 400, $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(StoresRequest $request)
    {
        $store = Stores::where([
            ['user_id', '=', Auth::user()->id],
            ['id', '=', $request->id],
        ])->first();

        if (!$store) {
            return $this->errorResponse(__("Store not found"), 404, null);
        }
        
        $product = Products::where('store_id', $store->id)->get();
        
        if(count($product) > 0){
            return $this->errorResponse(__("Cannot delelte store has products"), 404, null);
        }

        try {
            $id = $store->id;
            $store->delete();
            return $this->successResponse(['id' => $id], __('Delete success'));
        } catch (\Exception $e) {
            return $this->errorResponse(__("Internal Server Error"), 400, $e);
        }
    }
}
