<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsRequest;
use App\Models\Products;
use App\Models\Stores;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index(Request $request)
    {
        //
        $user = Auth::user();
        $listStoreId = Stores::where('user_id', $user->id)->pluck('id');

        $query = Products::whereIn('store_id', $listStoreId);

        if ($request->input('store_id', '') != '') {
            $searchStoreId = $request->input('store_id', '');
            if (in_array($searchStoreId, $listStoreId->toArray())) {
                $query->where('store_id', $searchStoreId);
            } else {
                return $this->errorResponse(__("Store not found"), 404, null);
            }
        };

        if ($request->input('name', '') != '') {
            $query->where('name', 'like', '%' . $request->input('name', '') . '%');
        };


        $data = $query->paginate($request->input('limit', 10));
        return $this->successResponse($data, __('Retrieving success'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(ProductsRequest $request)
    {
        $store = Stores::where([
            ['user_id', '=', Auth::user()->id],
            ['id', '=', $request->store_id],
        ])->first();

        if (!$store) {
            return $this->errorResponse(__("Store not found"), 404, null);
        }

        $product = [
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'store_id' =>  $store->id,
        ];

        try {
            $createProduct = Products::create(
                $product
            );
            return $this->successResponse(['id' => $createProduct->id], __('Create success'));
        } catch (\Exception $e) {
            return $this->errorResponse(__("Internal Server Error"), 400, $e);
        }
    }


    public function show(Request $request)
    {
        $product = Products::where([
            ['id', '=', $request->id],
        ])->first();

        if (!$this->checkProductIsInStoreUser($product)) {
            return $this->errorResponse(__("Product not found"), 404, null);
        }
        return $this->successResponse($product, __('Retrieving success'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductsRequest $request)
    {

        $product = Products::where([
            ['id', '=', $request->id],
        ])->first();

        if (!$this->checkProductIsInStoreUser($product)) {
            return $this->errorResponse(__("Product not found"), 404, null);
        }

        try {
            $product->update($request->except('store_id'));
            return $this->successResponse(['id' => $product->id], __('Update success'));
        } catch (\Exception $e) {
            return $this->errorResponse(__("Internal Server Error"), 400, $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductsRequest $request)
    {
        $product = Products::where([
            ['id', '=', $request->id],
        ])->first();

        if (!$this->checkProductIsInStoreUser($product)) {
            return $this->errorResponse(__("Product not found"), 404, null);
        }

        try {
            $id = $product->id;
            $product->delete();
            return $this->successResponse(['id' => $id], __('Delete success'));
        } catch (\Exception $e) {
            return $this->errorResponse(__("Internal Server Error"), 400, $e);
        }
    }

    public function checkProductIsInStoreUser($product)
    {
        $user = Auth::user();

        if (!$product) {
            return false;
        }
        if ($product->store->id != $user->id) {
            return false;
        }

        return true;
    }
}
