<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Http\Request;

class StoresRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = Request();
        $rules = [
            'name'          =>  'required|string|max:255',
        ];

        if ($request->method() === 'PUT') {
            $rules = [
                'id'          =>  'required|exists:stores,Id',
                'name'          =>  'required|string|max:255',
            ];
        }
        if ($request->method() === 'DELETE') {
            $rules = [
                'id'          =>  'required|exists:stores,Id',
            ];
        }
        return $rules;
    }
}
