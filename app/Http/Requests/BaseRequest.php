<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $response = [
            'Result'  => false,
            'Message' => $validator->errors()->first(),
            'Data'    => null,
        ];
        throw new HttpResponseException(response()->json($response, 400));
    }
}
