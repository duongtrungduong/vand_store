<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Http\Request;

class ProductsRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = Request();
        $rules = [
            'name'          =>  'required|string|max:255',
            'price'          =>  'integer|max:1000000',
            'store_id'         =>  'required|exists:stores,Id',
        ];

        if ($request->method() === 'PUT') {
            $id = $request->route('id');
            $rules = [
                'id'          =>  'required|exists:products,Id',
                'name'          =>  'required|string|max:255',
                'price'          =>  'integer|max:1000000',
            ];
        }
        if ($request->method() === 'DELETE') {
            $rules = [
                'id'          =>  'required|exists:products,Id',
            ];
        }
        return $rules;
    }
}
