-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: vand_store
-- ------------------------------------------------------
-- Server version	5.6.51

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2023_08_09_064049_create_stores_table',1),(6,'2023_08_09_064819_create_products_table',1),(7,'2023_08_09_090654_add_price_products_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) DEFAULT '0',
  `description` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_store_id_foreign` (`store_id`),
  CONSTRAINT `products_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,2,'Product1',380792,'Description-1','2023-08-09 16:50:35','2023-08-09 16:50:35'),(2,2,'Product2',857889,'Description-2','2023-08-09 16:50:35','2023-08-09 16:50:35'),(3,3,'Product3',105744,'Description-3','2023-08-09 16:50:35','2023-08-09 16:50:35'),(4,2,'Product4',650952,'Description-4','2023-08-09 16:50:35','2023-08-09 16:50:35'),(5,3,'Product5',143512,'Description-5','2023-08-09 16:50:35','2023-08-09 16:50:35'),(6,1,'Product6',659885,'Description-6','2023-08-09 16:50:35','2023-08-09 16:50:35'),(7,3,'Product7',520671,'Description-7','2023-08-09 16:50:35','2023-08-09 16:50:35'),(8,4,'Product8',637814,'Description-8','2023-08-09 16:50:35','2023-08-09 16:50:35'),(9,2,'Product9',920860,'Description-9','2023-08-09 16:50:35','2023-08-09 16:50:35'),(10,5,'Product10',906116,'Description-10','2023-08-09 16:50:35','2023-08-09 16:50:35'),(11,4,'Product11',267741,'Description-11','2023-08-09 16:50:35','2023-08-09 16:50:35'),(12,4,'Product12',51520,'Description-12','2023-08-09 16:50:35','2023-08-09 16:50:35'),(13,5,'Product13',480094,'Description-13','2023-08-09 16:50:35','2023-08-09 16:50:35'),(14,2,'Product14',954108,'Description-14','2023-08-09 16:50:35','2023-08-09 16:50:35'),(15,5,'Product15',313654,'Description-15','2023-08-09 16:50:35','2023-08-09 16:50:35'),(16,5,'Product16',270429,'Description-16','2023-08-09 16:50:35','2023-08-09 16:50:35'),(17,5,'Product17',114528,'Description-17','2023-08-09 16:50:35','2023-08-09 16:50:35'),(18,2,'Product18',446966,'Description-18','2023-08-09 16:50:35','2023-08-09 16:50:35'),(19,1,'Product19',586209,'Description-19','2023-08-09 16:50:35','2023-08-09 16:50:35'),(20,1,'Product20',52515,'Description-20','2023-08-09 16:50:35','2023-08-09 16:50:35'),(21,2,'Product21',38132,'Description-21','2023-08-09 16:50:35','2023-08-09 16:50:35'),(22,5,'Product22',754341,'Description-22','2023-08-09 16:50:35','2023-08-09 16:50:35'),(23,1,'Product23',791681,'Description-23','2023-08-09 16:50:35','2023-08-09 16:50:35'),(24,2,'Product24',374174,'Description-24','2023-08-09 16:50:35','2023-08-09 16:50:35'),(25,1,'Product25',445910,'Description-25','2023-08-09 16:50:35','2023-08-09 16:50:35'),(26,3,'Product26',465778,'Description-26','2023-08-09 16:50:35','2023-08-09 16:50:35'),(27,1,'Product27',762034,'Description-27','2023-08-09 16:50:35','2023-08-09 16:50:35'),(28,4,'Product28',758995,'Description-28','2023-08-09 16:50:35','2023-08-09 16:50:35'),(29,5,'Product29',654867,'Description-29','2023-08-09 16:50:35','2023-08-09 16:50:35'),(30,2,'Product30',231959,'Description-30','2023-08-09 16:50:35','2023-08-09 16:50:35'),(31,5,'Product31',944453,'Description-31','2023-08-09 16:50:35','2023-08-09 16:50:35'),(32,3,'Product32',356583,'Description-32','2023-08-09 16:50:35','2023-08-09 16:50:35'),(33,5,'Product33',869561,'Description-33','2023-08-09 16:50:35','2023-08-09 16:50:35'),(34,3,'Product34',516626,'Description-34','2023-08-09 16:50:35','2023-08-09 16:50:35'),(35,2,'Product35',675328,'Description-35','2023-08-09 16:50:35','2023-08-09 16:50:35'),(36,1,'Product36',41749,'Description-36','2023-08-09 16:50:35','2023-08-09 16:50:35'),(37,4,'Product37',960928,'Description-37','2023-08-09 16:50:35','2023-08-09 16:50:35'),(38,2,'Product38',87590,'Description-38','2023-08-09 16:50:35','2023-08-09 16:50:35'),(39,1,'Product39',386443,'Description-39','2023-08-09 16:50:35','2023-08-09 16:50:35'),(40,3,'Product40',197586,'Description-40','2023-08-09 16:50:35','2023-08-09 16:50:35'),(41,3,'Product41',999511,'Description-41','2023-08-09 16:50:35','2023-08-09 16:50:35'),(42,5,'Product42',470429,'Description-42','2023-08-09 16:50:35','2023-08-09 16:50:35'),(43,4,'Product43',659495,'Description-43','2023-08-09 16:50:35','2023-08-09 16:50:35'),(44,3,'Product44',351181,'Description-44','2023-08-09 16:50:35','2023-08-09 16:50:35'),(45,2,'Product45',868371,'Description-45','2023-08-09 16:50:35','2023-08-09 16:50:35'),(46,3,'Product46',119734,'Description-46','2023-08-09 16:50:35','2023-08-09 16:50:35'),(47,2,'Product47',705020,'Description-47','2023-08-09 16:50:35','2023-08-09 16:50:35'),(48,5,'Product48',744080,'Description-48','2023-08-09 16:50:35','2023-08-09 16:50:35'),(49,3,'Product49',759465,'Description-49','2023-08-09 16:50:35','2023-08-09 16:50:35'),(50,3,'Product50',561170,'Description-50','2023-08-09 16:50:35','2023-08-09 16:50:35'),(51,1,'Product51',332075,'Description-51','2023-08-09 16:50:35','2023-08-09 16:50:35'),(52,1,'Product52',947075,'Description-52','2023-08-09 16:50:35','2023-08-09 16:50:35'),(53,2,'Product53',89660,'Description-53','2023-08-09 16:50:35','2023-08-09 16:50:35'),(54,4,'Product54',132771,'Description-54','2023-08-09 16:50:35','2023-08-09 16:50:35'),(55,1,'Product55',502502,'Description-55','2023-08-09 16:50:35','2023-08-09 16:50:35'),(56,2,'Product56',900409,'Description-56','2023-08-09 16:50:35','2023-08-09 16:50:35'),(57,1,'Product57',114593,'Description-57','2023-08-09 16:50:35','2023-08-09 16:50:35'),(58,2,'Product58',861375,'Description-58','2023-08-09 16:50:35','2023-08-09 16:50:35'),(59,3,'Product59',828827,'Description-59','2023-08-09 16:50:35','2023-08-09 16:50:35'),(60,4,'Product60',396045,'Description-60','2023-08-09 16:50:35','2023-08-09 16:50:35'),(61,4,'Product61',331107,'Description-61','2023-08-09 16:50:35','2023-08-09 16:50:35'),(62,4,'Product62',547275,'Description-62','2023-08-09 16:50:35','2023-08-09 16:50:35'),(63,1,'Product63',373155,'Description-63','2023-08-09 16:50:35','2023-08-09 16:50:35'),(64,4,'Product64',17871,'Description-64','2023-08-09 16:50:35','2023-08-09 16:50:35'),(65,2,'Product65',356960,'Description-65','2023-08-09 16:50:35','2023-08-09 16:50:35'),(66,5,'Product66',578431,'Description-66','2023-08-09 16:50:35','2023-08-09 16:50:35'),(67,5,'Product67',814990,'Description-67','2023-08-09 16:50:35','2023-08-09 16:50:35'),(68,1,'Product68',690341,'Description-68','2023-08-09 16:50:35','2023-08-09 16:50:35'),(69,5,'Product69',694101,'Description-69','2023-08-09 16:50:35','2023-08-09 16:50:35'),(70,3,'Product70',937569,'Description-70','2023-08-09 16:50:35','2023-08-09 16:50:35'),(71,5,'Product71',469821,'Description-71','2023-08-09 16:50:35','2023-08-09 16:50:35'),(72,1,'Product72',377204,'Description-72','2023-08-09 16:50:35','2023-08-09 16:50:35'),(73,1,'Product73',422670,'Description-73','2023-08-09 16:50:35','2023-08-09 16:50:35'),(74,1,'Product74',590855,'Description-74','2023-08-09 16:50:35','2023-08-09 16:50:35'),(75,3,'Product75',65668,'Description-75','2023-08-09 16:50:35','2023-08-09 16:50:35'),(76,3,'Product76',742596,'Description-76','2023-08-09 16:50:35','2023-08-09 16:50:35'),(77,2,'Product77',208222,'Description-77','2023-08-09 16:50:35','2023-08-09 16:50:35'),(78,2,'Product78',829933,'Description-78','2023-08-09 16:50:35','2023-08-09 16:50:35'),(79,5,'Product79',422913,'Description-79','2023-08-09 16:50:35','2023-08-09 16:50:35'),(80,2,'Product80',543169,'Description-80','2023-08-09 16:50:35','2023-08-09 16:50:35'),(81,3,'Product81',745778,'Description-81','2023-08-09 16:50:35','2023-08-09 16:50:35'),(82,3,'Product82',136647,'Description-82','2023-08-09 16:50:35','2023-08-09 16:50:35'),(83,4,'Product83',703097,'Description-83','2023-08-09 16:50:35','2023-08-09 16:50:35'),(84,3,'Product84',267443,'Description-84','2023-08-09 16:50:35','2023-08-09 16:50:35'),(85,5,'Product85',622410,'Description-85','2023-08-09 16:50:35','2023-08-09 16:50:35'),(86,2,'Product86',212139,'Description-86','2023-08-09 16:50:35','2023-08-09 16:50:35'),(87,2,'Product87',928203,'Description-87','2023-08-09 16:50:35','2023-08-09 16:50:35'),(88,1,'Product88',455643,'Description-88','2023-08-09 16:50:35','2023-08-09 16:50:35'),(89,2,'Product89',943000,'Description-89','2023-08-09 16:50:35','2023-08-09 16:50:35'),(90,1,'Product90',433457,'Description-90','2023-08-09 16:50:35','2023-08-09 16:50:35'),(91,2,'Product91',948007,'Description-91','2023-08-09 16:50:35','2023-08-09 16:50:35'),(92,5,'Product92',163912,'Description-92','2023-08-09 16:50:35','2023-08-09 16:50:35'),(93,1,'Product93',370060,'Description-93','2023-08-09 16:50:35','2023-08-09 16:50:35'),(94,5,'Product94',952325,'Description-94','2023-08-09 16:50:35','2023-08-09 16:50:35'),(95,5,'Product95',240155,'Description-95','2023-08-09 16:50:35','2023-08-09 16:50:35'),(96,1,'Product96',411785,'Description-96','2023-08-09 16:50:35','2023-08-09 16:50:35'),(97,3,'Product97',616642,'Description-97','2023-08-09 16:50:35','2023-08-09 16:50:35'),(98,4,'Product98',553886,'Description-98','2023-08-09 16:50:35','2023-08-09 16:50:35'),(99,5,'Product99',291235,'Description-99','2023-08-09 16:50:35','2023-08-09 16:50:35'),(100,4,'Product100',33373,'Description-100','2023-08-09 16:50:35','2023-08-09 16:50:35'),(101,3,'Product101',909968,'Description-101','2023-08-09 16:50:35','2023-08-09 16:50:35'),(102,3,'Product102',802087,'Description-102','2023-08-09 16:50:35','2023-08-09 16:50:35'),(103,4,'Product103',627519,'Description-103','2023-08-09 16:50:35','2023-08-09 16:50:35'),(104,2,'Product104',362312,'Description-104','2023-08-09 16:50:35','2023-08-09 16:50:35'),(105,3,'Product105',489665,'Description-105','2023-08-09 16:50:35','2023-08-09 16:50:35'),(106,4,'Product106',426413,'Description-106','2023-08-09 16:50:35','2023-08-09 16:50:35'),(107,3,'Product107',201614,'Description-107','2023-08-09 16:50:35','2023-08-09 16:50:35'),(108,4,'Product108',965201,'Description-108','2023-08-09 16:50:35','2023-08-09 16:50:35'),(109,1,'Product109',442405,'Description-109','2023-08-09 16:50:35','2023-08-09 16:50:35'),(110,1,'Product110',41044,'Description-110','2023-08-09 16:50:35','2023-08-09 16:50:35'),(111,4,'Product111',416765,'Description-111','2023-08-09 16:50:35','2023-08-09 16:50:35'),(112,5,'Product112',846840,'Description-112','2023-08-09 16:50:35','2023-08-09 16:50:35'),(113,3,'Product113',461255,'Description-113','2023-08-09 16:50:35','2023-08-09 16:50:35'),(114,5,'Product114',727821,'Description-114','2023-08-09 16:50:35','2023-08-09 16:50:35'),(115,1,'Product115',605490,'Description-115','2023-08-09 16:50:35','2023-08-09 16:50:35'),(116,2,'Product116',357375,'Description-116','2023-08-09 16:50:35','2023-08-09 16:50:35'),(117,2,'Product117',869857,'Description-117','2023-08-09 16:50:35','2023-08-09 16:50:35'),(118,4,'Product118',242919,'Description-118','2023-08-09 16:50:35','2023-08-09 16:50:35'),(119,5,'Product119',208818,'Description-119','2023-08-09 16:50:35','2023-08-09 16:50:35'),(120,4,'Product120',632220,'Description-120','2023-08-09 16:50:35','2023-08-09 16:50:35'),(121,2,'Product121',990909,'Description-121','2023-08-09 16:50:35','2023-08-09 16:50:35'),(122,1,'Product122',941698,'Description-122','2023-08-09 16:50:35','2023-08-09 16:50:35'),(123,1,'Product123',204702,'Description-123','2023-08-09 16:50:35','2023-08-09 16:50:35'),(124,5,'Product124',734250,'Description-124','2023-08-09 16:50:35','2023-08-09 16:50:35'),(125,4,'Product125',257256,'Description-125','2023-08-09 16:50:35','2023-08-09 16:50:35'),(126,3,'Product126',142111,'Description-126','2023-08-09 16:50:35','2023-08-09 16:50:35'),(127,4,'Product127',503811,'Description-127','2023-08-09 16:50:35','2023-08-09 16:50:35'),(128,4,'Product128',715216,'Description-128','2023-08-09 16:50:35','2023-08-09 16:50:35'),(129,1,'Product129',203499,'Description-129','2023-08-09 16:50:35','2023-08-09 16:50:35'),(130,5,'Product130',720982,'Description-130','2023-08-09 16:50:35','2023-08-09 16:50:35'),(131,4,'Product131',152755,'Description-131','2023-08-09 16:50:35','2023-08-09 16:50:35'),(132,2,'Product132',802335,'Description-132','2023-08-09 16:50:35','2023-08-09 16:50:35'),(133,3,'Product133',250607,'Description-133','2023-08-09 16:50:35','2023-08-09 16:50:35'),(134,3,'Product134',43746,'Description-134','2023-08-09 16:50:35','2023-08-09 16:50:35'),(135,4,'Product135',477783,'Description-135','2023-08-09 16:50:35','2023-08-09 16:50:35'),(136,3,'Product136',265971,'Description-136','2023-08-09 16:50:35','2023-08-09 16:50:35'),(137,2,'Product137',540455,'Description-137','2023-08-09 16:50:35','2023-08-09 16:50:35'),(138,2,'Product138',296117,'Description-138','2023-08-09 16:50:35','2023-08-09 16:50:35'),(139,3,'Product139',739612,'Description-139','2023-08-09 16:50:35','2023-08-09 16:50:35'),(140,5,'Product140',974936,'Description-140','2023-08-09 16:50:35','2023-08-09 16:50:35'),(141,1,'Product141',709467,'Description-141','2023-08-09 16:50:35','2023-08-09 16:50:35'),(142,5,'Product142',771839,'Description-142','2023-08-09 16:50:35','2023-08-09 16:50:35'),(143,1,'Product143',254543,'Description-143','2023-08-09 16:50:35','2023-08-09 16:50:35'),(144,1,'Product144',319834,'Description-144','2023-08-09 16:50:35','2023-08-09 16:50:35'),(145,4,'Product145',300289,'Description-145','2023-08-09 16:50:35','2023-08-09 16:50:35'),(146,4,'Product146',611784,'Description-146','2023-08-09 16:50:35','2023-08-09 16:50:35'),(147,5,'Product147',532872,'Description-147','2023-08-09 16:50:35','2023-08-09 16:50:35'),(148,4,'Product148',73979,'Description-148','2023-08-09 16:50:35','2023-08-09 16:50:35'),(149,5,'Product149',899208,'Description-149','2023-08-09 16:50:35','2023-08-09 16:50:35'),(150,3,'Product150',222558,'Description-150','2023-08-09 16:50:35','2023-08-09 16:50:35');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stores_user_id_foreign` (`user_id`),
  CONSTRAINT `stores_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,1,'Store1','Address1','2023-08-09 16:50:35','2023-08-09 16:50:35'),(2,1,'Store2','Address2','2023-08-09 16:50:35','2023-08-09 16:50:35'),(3,2,'Store3','Address3','2023-08-09 16:50:35','2023-08-09 16:50:35'),(4,3,'Store4','Address4','2023-08-09 16:50:35','2023-08-09 16:50:35'),(5,2,'Store5','Address5','2023-08-09 16:50:35','2023-08-09 16:50:35');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin1','$2y$10$9snauQuAG0ncJsLU17hsTeUmu4JwcUlsMYp96fq/iOjsA7mA.FIL2','2023-08-09 16:50:34','2023-08-09 16:50:34'),(2,'admin2','$2y$10$PGnpoEUOu8c/7nVCrSs5JOksQfxI.rMEoXE87cOLa6COZB3x31IDq','2023-08-09 16:50:34','2023-08-09 16:50:34'),(3,'admin3','$2y$10$/QkMQFg2YvSV9QMrHTdGU.6n7aMArPRSOIz96HgQOsQ57DGQs804G','2023-08-09 16:50:34','2023-08-09 16:50:34');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-10  1:02:29
