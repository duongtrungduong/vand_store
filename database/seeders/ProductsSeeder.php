<?php

namespace Database\Seeders;

use App\Models\Products;
use App\Models\Stores;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 150; $i++) {
            $product = [
                "store_id" =>  Stores::all()->random()->id,
                "name" => 'Product' . $i,
                "description" => 'Description-' . $i,
                "price" => rand(1000, 1000000),
            ];

            Products::create($product);
        }
    }
}
