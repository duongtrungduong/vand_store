<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username'     =>  'admin1',
                'password'     =>  Hash::make(123456),
            ],
            [
                'username'     =>  'admin2',
                'password'     =>  Hash::make(123456),
            ],
            [
                'username'     =>  'admin3',
                'password'     =>  Hash::make(123456),
            ],
                    
        ];
        foreach($users as $key=>$user){
            User::create($user);
        }
    }
}
