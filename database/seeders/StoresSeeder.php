<?php

namespace Database\Seeders;

use App\Models\Stores;
use App\Models\User;
use Illuminate\Database\Seeder;

class StoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 5; $i++) {
            $store = [
                "user_id" =>  User::all()->random()->id,
                "name" => 'Store' . $i,
                "address" => 'Address' . $i,
            ];

            Stores::create($store);
        }
    }
}
